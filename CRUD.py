from tkinter import *
from tkinter import messagebox
import sqlite3
#--------- Funciones botones -------

def conectarBase():

    conexion=sqlite3.connect("Productos")
    cursor=conexion.cursor()
    try:
        cursor.execute('''
        CREATE TABLE DATOSPRODUCTOS(
            ID INTEGER PRIMARY KEY AUTOINCREMENT,
            NOMBRE_PRODUCTO VARCHAR(50),
            OUT VARCHAR(50),
            MARCA VARCHAR(10),
            FAMILIA VARCHAR(50),
            COMENTARIOS VARCHAR(100)
            )  
        ''')
        messagebox.showinfo("Practica de Sergio y Neftaly :3", "¡Base de datos creada con éxito!")
    except sqlite3.OperationalError:
        messagebox.showwarning("Practica de Sergio y Neftaly :3","LA BASE DE DATOS YA EXISTE")

    #conexion.close()

def funcionSalir():
    salir=messagebox.askyesno("Salir", "¿Deseas salir de la aplicación?")
    if salir:
        raiz.destroy()

def borrarCampos():
    idVar.set("")
    productoVar.set("")
    UOTVar.set("")
    marcaVar.set("")
    familiaVar.set("")
    commentText.delete(1.0, END)

#Crear Registros
def createR():
    conexion=sqlite3.connect("Productos")
    cursor=conexion.cursor()
    #Se recoge la informacion de las VARIABLES de los ENTRY
    #Se concatenan las variables en la instruccion SQL
    """cursor.execute("INSERT INTO DATOSPRODUCTOS VALUES (NULL, '" + productoVar.get() +
    "','"+ UOTVar.get()+
    "','"+ marcaVar.get()+
    "','"+familiaVar.get()+
    "','"+commentText.get("1.0",END)+"')")"""

    #CONSULTA PARAMETRIZADA
    if (productoVar.get()=="" or UOTVar.get()=="" or marcaVar.get()=="" or familiaVar.get()==""):
        messagebox.showwarning("ATENCIÓN", "UN CAMPO ESTÁ VACIO.")
    else:
        datos=productoVar.get(),UOTVar.get(),marcaVar.get(),familiaVar.get(),commentText.get("1.0",END)
        cursor.execute("INSERT INTO DATOSPRODUCTOS VALUES (NULL,?,?,?,?,?)",datos) 
        conexion.commit()
        messagebox.showinfo("BBDD","Registro creado con éxito.")

def readR():
    conexion=sqlite3.connect("Productos")
    cursor=conexion.cursor()
    try:
        cursor.execute("SELECT * FROM DATOSPRODUCTOS WHERE ID="+idVar.get())
        #Crear una lista de 1 tupla con los datos del usuario EJEMPLO
        datos=cursor.fetchall()
        #print(datos)
        productoVar.set(datos[0][1])
        UOTVar.set(datos[0][2])
        marcaVar.set(datos[0][3])
        familiaVar.set(datos[0][4])
        commentText.delete(1.0, END)
        commentText.insert(1.0,datos[0][5])
    except IndexError:
        messagebox.showwarning("ATENCIÓN", "REGISTRO NO ENCONTRADO.")
    except sqlite3.OperationalError:
        messagebox.showwarning("ATENCIÓN", "INGRESA UN ID VÁLIDO.")

    conexion.commit()


def updateR():
    conexion=sqlite3.connect("Productos")
    cursor=conexion.cursor()
    #Se recoge la informacion de las VARIABLES de los ENTRY
    #Se concatenan las variables en la instruccion SQL
    """cursor.execute("UPDATE DATOSPRODUCTOS SET NOMBRE_PRODUCTO='"+ productoVar.get()+
    "', OUT='" + UOTVar.get() +
    "', MARCA='" + marcaVar.get() +
    "', FAMILIA='"+familiaVar.get()+
    "', COMENTARIOS='"+commentText.get("1.0", END)+
    "' WHERE ID="+ idVar.get())"""
    
    try:
        #CONSULTA PARAMETRIZADA
        datos=productoVar.get(),UOTVar.get(),marcaVar.get(),familiaVar.get(),commentText.get("1.0",END)
        cursor.execute("UPDATE DATOSPRODUCTOS SET NOMBRE_PRODUCTO=?, OUT=?, MARCA=?, FAMILIA=?, COMENTARIOS=? WHERE ID="+idVar.get() ,(datos))
        conexion.commit()
        messagebox.showinfo("BBDD","Registro actualizado con éxito.")  
    except sqlite3.OperationalError:
        messagebox.showwarning("ATENCIÓN", "INGRESA UN ID VÁLIDO.")  

def deleteR():
    conexion=sqlite3.connect("Productos")
    cursor=conexion.cursor()
    try:
        cursor.execute("DELETE FROM DATOSPRODUCTOS WHERE ID= "+ idVar.get())
        conexion.commit()
        messagebox.showinfo("BBDD","Registro eliminado con éxito.")
    except sqlite3.OperationalError:
        messagebox.showwarning("ATENCIÓN", "INGRESA UN ID VÁLIDO.")    

def about():
    messagebox.showinfo("Practica de Sergio y Neftaly :3", "Esta practica ha sido realizada para el Curso de GIT \n 2023 ©Todos los derechos reservados")


#------------Raíz------------
raiz=Tk()
raiz.title("Sistema de Inventario")
#raiz.iconbitmap("xbox.ico")
raiz.resizable(False,False)
raiz.geometry("300x380")#(AnchoxAlto)

#---------Frame Principal------------
mainFrame=Frame()
mainFrame.pack(fill="both", expand=True)
mainFrame.config(bg="pink")
#mainFrame.config(width="250",height="350")

#-----------Menú superior-----------
barraMenu=Menu(raiz)
raiz.config(menu=barraMenu)

#Submenus 
bbddMenu=Menu(barraMenu, tearoff=0)
bbddMenu.add_command(label="Conectar", command=conectarBase)
bbddMenu.add_command(label="Salir", command=funcionSalir)

borrarMenu=Menu(barraMenu, tearoff=0)
borrarMenu.add_command(label="Borrar campos", command=borrarCampos)

CRUDMenu=Menu(barraMenu, tearoff=False)
CRUDMenu.add_command(label="Crear",command=createR)
CRUDMenu.add_command(label="Leer",command=readR)
CRUDMenu.add_command(label="Actualizar", command=updateR)
CRUDMenu.add_command(label="Borrar", command=deleteR)

ayudaMenu=Menu(barraMenu, tearoff=0)
ayudaMenu.add_command(label="Acerca de...", command=about)

#Agregar botones a la barra menu
barraMenu.add_cascade(label="BBDD", menu=bbddMenu)
barraMenu.add_cascade(label="Borrar", menu=borrarMenu)
barraMenu.add_cascade(label="Acciones", menu=CRUDMenu)
barraMenu.add_cascade(label="Ayuda", menu=ayudaMenu)


#------------- INTERFAZ PRINCIPAL -----------------
idVar=StringVar()
productoVar=StringVar()
marcaVar=StringVar()
UOTVar=StringVar()
familiaVar=StringVar()

Label(mainFrame, text="ID").grid(row=0, column=0,sticky="e")
idEntry=Entry(mainFrame, textvariable=idVar)
idEntry.grid(row=0, column=1, padx=10, pady=10, columnspan=2,sticky="w")

Label(mainFrame, text="Producto").grid(row=1, column=0,sticky="e")
nombreEntry=Entry(mainFrame, textvariable=productoVar)
nombreEntry.grid(row=1, column=1, padx=10, pady=10, columnspan=2,sticky="w")

Label(mainFrame, text="UOT").grid(row=2, column=0,sticky="e")
passwordEntry=Entry(mainFrame, textvariable=UOTVar)
passwordEntry.grid(row=2, column=1, padx=10, pady=10, columnspan=2,sticky="w")
#passwordEntry.config(show="*")

Label(mainFrame, text="Marca").grid(row=3, column=0,sticky="e")
apellidoEntry=Entry(mainFrame, textvariable=marcaVar)
apellidoEntry.grid(row=3, column=1, padx=10, pady=10, columnspan=2,sticky="w")

Label(mainFrame, text="Familia").grid(row=4, column=0,sticky="e")
addressEntry=Entry(mainFrame, textvariable=familiaVar)
addressEntry.grid(row=4, column=1, padx=10, pady=10, columnspan=2,sticky="w")

Label(mainFrame, text="Comentarios").grid(row=5, column=0,sticky="e")
commentText=Text(mainFrame, width=16, height=5)
commentText.grid(row=5, column=1, padx=10, pady=10 )
scrollVert=Scrollbar(mainFrame, command=commentText.yview)
scrollVert.grid(row=5, column=2, sticky="nsew")
commentText.config(yscrollcommand=scrollVert.set)

#--------- Botones inferiores -------------
botonFrame=Frame()
botonFrame.pack(fill="both", expand=True)
botonFrame.config(bg="white")

botonCreate=Button(botonFrame, text="Create", command=createR)
botonCreate.grid(row=0,column=0, padx=10, pady=10)

botonRead=Button(botonFrame, text="Read", command=readR)
botonRead.grid(row=0,column=1, padx=10, pady=10)

botonUpdate=Button(botonFrame, text="Update", command=updateR)
botonUpdate.grid(row=0,column=2, padx=10, pady=10)

botonDelete=Button(botonFrame, text="Delete", command=deleteR)
botonDelete.grid(row=0,column=3, padx=10, pady=10)


raiz.mainloop()